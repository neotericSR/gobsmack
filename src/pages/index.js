import React, { Component } from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero from '../components/Hero'
import ProgressBarTag from "../components/ProgressBarTag"
import ProgressBar from "../components/ProgressBar"
import "../styles/index.scss"
import Axios from "axios"

const api = Axios.create({
  method: 'get',
  baseURL: 'http://localhost:3000/'
})

class IndexPage extends Component {

  constructor() {
    super();
    this.state = {
      name: '',
      value: 0,
      goal: 0,
      loading: false
    }
  }

  componentWillMount() {
    this.getData();
  }

  getData = async () => {
    this.setState({ loading: true });
    try {
      let data = await api.get(`api/gobsmackTest`).then(({ data }) => data);
      this.setState({ name: data.firstName, value: data.walletValue, goal: data.walletTarget });
    } catch (exp) {
      console.error(exp);
    } finally {
      this.setState({ loading: false });
    }
  }

  render() {
    return (
      <Layout>
        <SEO title="Home" />
        <div className="home">
          <section className="home__hero">
            <Hero customerName={this.state.name} />
          </section>
          <section className="home__image">
          </section>
          <div className="home__bar">
            <ProgressBar value={this.state.value} goal={this.state.goal} />
          </div>
          <div className="home__bar--tag">
            <ProgressBarTag value={this.state.value} goal={this.state.goal} />
          </div>
        </div>
      </Layout>
    )
  }
}

export default IndexPage
