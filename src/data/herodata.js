let hero = {
    name: 'James',
    title: 'Welcome',
    main: 'More cash in your wallet from everyday shopping',
    submain: 'Shop for things you’d buy anyway with over 3,000 brands – online and in store – and watch the cash stack up in your wallet.'
}

export let herobutton = {
    url: "/",
    color: "#D71440",
    text: "Activate now",
}

export let bar = {
    color: "#D71440",
    goal: 300,
    value: 200.1,
}

export default hero