import React from 'react'
import {Link} from 'gatsby'
import PropTypes from "prop-types"
import {herobutton} from '../data/herodata'
import '../styles/button.scss'
import Arrow from '../images/arrow.svg'

const HeroButton = ({ buttonURL, buttonText }) => {

return <Link to={`${buttonURL}`}>
<button className='herobutton'>{buttonText} <Arrow /></button>
</Link>

}

HeroButton.propTypes = 
{
    buttonURL: PropTypes.string,
    buttonText: PropTypes.string,
}

HeroButton.defaultProps = 
{
    buttonURL: herobutton.url,
    buttonText: herobutton.text,
}

export default HeroButton