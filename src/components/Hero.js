import React from 'react';
import PropTypes from 'prop-types'
// import { Link } from 'gatsby'
import herodata from '../data/herodata'
import '../styles/hero.scss'
import HeroButton from "./HeroButton"

const Hero = ({ customerName }) => (
    <div className="hero">
        <h5 className='hero__title'>{herodata.title} {customerName}</h5>
        <p className='hero__main'>{herodata.main}</p>
        <p className='hero__submain'>{herodata.submain}</p>
        <HeroButton />
    </div>
)

Hero.propTypes = {
    customerName: PropTypes.string,
}

Hero.defaultProps = {
    customerName: herodata.name,
}

export default Hero