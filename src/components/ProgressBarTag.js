import React from 'react'
import PropTypes from "prop-types"
import {bar} from '../data/herodata'
import Icon from '../images/wallet.svg'
import '../styles/ProgressBarTag.scss'

const ProgressBarTag = ({ value, goal }) => (
        <section className="progressBarTag">
            <figure className="progressBarTag__figure">
                <div className="progressBarTag__wallet"><Icon /></div>
                <figcaption className="progressBarTag__caption--1">
                    &#163;{Math.floor(value)}<span className="progressBarTag__decimal">.{(value % 1).toFixed(2).substr(2)}</span>
                </figcaption>
                <figcaption className="progressBarTag__caption--2">
                    Goal: &#163;{goal}
                </figcaption>
            </figure>
        </section>
)

ProgressBarTag.propTypes = 
{
    value: PropTypes.number,
    goal: PropTypes.number,
}

ProgressBarTag.defaultProps = 
{
    value: bar.value,
    goal: bar.goal,
}

export default ProgressBarTag