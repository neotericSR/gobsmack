import React from 'react'
import '../styles/progressBar.scss'
import PropTypes from "prop-types"

const ProgressBar = ({value, goal}) => (
        <svg viewBox="0 0 600 200" className='progressBarSVG'>
        <path d="M0,200
           Q300,150 600,200
           T600 200"
                fill="none" stroke="#D5D6D8" strokeWidth="2px"/>
        <path d="M0,200
           Q300,150 600,200
           T600 200"
                fill="none" stroke="#36ED79" strokeWidth="2px" strokeDasharray={`${ value/goal * 625} 625`}/>
            <path d="M0,200
           Q300,150 600,200
           T600 200 Z"
                fill="white" stroke="none" strokeWidth="2px" />
        </svg>)

ProgressBar.propTypes = 
{
    barvalue: PropTypes.number,
}

ProgressBar.defaultProps = 
{
    barvalue: 0,
}

export default ProgressBar;