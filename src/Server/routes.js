var express = require('express');
var axios = require('axios');

const api = axios.create({
    method: 'get',
    baseURL: "http://buildingbrands.co/"
})

let dataObj = 'loading...';

const getLists = async() => {
    try {
        let data = await api.get(`react-test/wallet.json`).then(({ data }) => data);
        dataObj = data;
    } catch (exp) {
        console.error(exp);
    }
}

const router = express.Router();

router.get('/api/gobsmackTest', async(req, res) => {
    getLists()
    try {
        res.json(dataObj);
        console.log(dataObj)
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
})

module.exports = router