var express = require('express');
var apiRoute = require('./routes')

var app = express();

//using express as middle ware
app.use(function(req, res, next) {

    // Is a CORS (cross origin resource sharing)
    res.header("Access-Control-Allow-Origin", "*");

    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // invoked to execute middle ware
    next();
});
app.use(express.static('public'))
app.use(apiRoute)

const port = process.env.port || 3000
app.listen(port, () => {
    console.log(`Server listening on port: ${port}`)
})